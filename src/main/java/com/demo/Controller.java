package com.demo;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class Controller {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Value("${queue.name}")
    private String queueName;

    @GetMapping("/hello")
    public void sendMessage() {
        rabbitTemplate.convertAndSend(queueName,"Hello World");
    }

    @GetMapping("/currency")
    public String getCurrencyList() {
        RestTemplate restTemplate = new RestTemplate();
        return restTemplate.getForObject("https://api.fixer.io/latest", String.class);
    }

}
