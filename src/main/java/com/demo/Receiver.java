package com.demo;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;


@Component
public class Receiver {

    @RabbitListener(queues = "spring-boot-queue")
    public void receiveMessage(String message) {
        System.out.println("Received <" + message + ">");
    }

}